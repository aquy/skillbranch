import express from 'express';

const app = express();

app.get('/', function(req,res) {
    const a = validateNumber(req.query.a);
    const b = validateNumber(req.query.b);
    const sum = (a + b);
    return res.send('Вывод: ' + sum);
//    res.send('Hello World!');
});

app.listen(3001, function(){
    console.log('app on port 3001');
});

function validateNumber(n) {
    if (!isNaN(parseFloat(n)) && isFinite(n)) {
        return parseInt(n);
    }
    return 0;
}

