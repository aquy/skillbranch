import express from 'express';

const app = express();

app.get('/', function(req,res) {
    const fullname = req.query.fullname.replace(/\s{2,}/g, " ").replace(/([.!?]+)(?=\S)/g, "$1 ").split(' ');
    if (fullname.length === 3) {
        return res.send(fullname[2] + ' ' + fullname[0][0] + '. ' + fullname[1][0] + '.');
    }
    if (fullname.length === 2) {
        return res.send(fullname[1] + ' ' + fullname[1][0] + '.');
    }
    if (fullname.length === 1) {
        return res.send(fullname[0]);
    }
    return res.send('Invalid fullname');
});

app.listen(3001, function(){
    console.log('app on port 3001');
});