import express from 'express';

const app = express();

app.get('/', function(req,res) {
    const username = req.query.username;
    if (username) {
        return res.send('@' + canonize(username));
    }
    return res.send('not found');
});

app.listen(3001, function(){
    console.log('app on port 3001');
});

function canonize(url) {
    const re = new RegExp('@?(https?:)?(\/\/)?((twitter|vk|telegram)[^\/]*\/)?([a-zA-Z0-9]*)');
    const username = url.match(re);
    return username[5];
}