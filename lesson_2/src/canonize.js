function canonize(url) {
    const re = new RegExp('@?(https?:)?(\/\/)?((twitter|vk|telegram)[^\/]*\/])?([a-zA-Z0-9]*)', 'ig');
    const username = url.match(re);
    return username;
}

const array= [
    'https://vk.com/igor.suvorov',
    'https://twitter.com/suvorovigor',
    'https://telegram.me/skillbranch',
    '@skillbranch',
    'https://vk.com/skillbranch?w=wall-117903599_1076'
];

array.forEach(function(url){
    const username = canonize(url);
    console.log(username[6]);
});
